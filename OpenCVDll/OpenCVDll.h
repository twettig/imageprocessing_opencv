#pragma once



struct Color32;
struct Coordinate {
	float x, y;
};


//extern "C" __declspec(dllexport) void initCamera();

extern "C" __declspec(dllexport) void processImage(Color32** raw, const int width, const int height);




/*** Camera calibration & Automatic image correction ***/

extern "C" __declspec(dllexport) bool startCameraCalibration(const char* imageDir, const char* cameraCalibrationFilePath, const int imageWidth, const int imageHeight);
extern "C" __declspec(dllexport) void findChessboardCorners(Color32** rawImg, const int width, const int height);
//extern "C" __declspec(dllexport) void loadCalibrationParameters(const String filePath, Mat& cameraMatrix, Mat& distanceCoefficients, Mat& undistortMatrix, Rect& undistortArea);
//extern "C" __declspec(dllexport) void correctImg(Color32 * *rawImg, const int width, const int height, const string cameraCalibrationPath, const float* worldPoints, const int worldPointsLength, const float* screenPoints, const int screenPointsLength);


/*** Utilities ***/

extern "C" __declspec(dllexport) void flipImg(Color32** rawImg, const int width, const int height, const bool x_axis, const bool y_axis);
extern "C" __declspec(dllexport) void rotateScaleImg(Color32** rawImg, const int width, const int height, const float angle = 0.0, const float scale = 1.0);
extern "C" __declspec(dllexport) void perspectiveWarpImg(Color32 * *rawImg, const int width, const int height, const float* worldPoints, const int worldPointsLength, const float* screenPoints, const int screenPointsLength);



/*** Marker Detection ***/
extern "C" __declspec(dllexport) void getMarkerCoordinates(Color32 * *rawImg, const int width, const int height, int* markerId, float* TR_x, float* TR_y);
//extern "C" __declspec(dllexport) void getMarkerCoordinates(Color32 * *rawImg, const int width, const int height, int* markerId, Coordinate cornerTL, Coordinate cornerTR, Coordinate cornerBR, Coordinate cornerBL);