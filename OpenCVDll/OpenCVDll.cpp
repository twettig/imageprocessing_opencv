#include "pch.h"
#include "OpenCVDll.h"

#include <cstdio>
#include <format>
#include <fstream>
#include <string>

#include <opencv2/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/aruco.hpp>


using namespace std;
using namespace cv;

struct Color32
{
    uchar red;
    uchar green;
    uchar blue;
    uchar alpha;
};

static Mat rotation_matrix;
static Point2f image_center_point;
static Mat output_img;

/*
VideoCapture cameraStreamCapture;
String windowName = "Unity_Test";


int initCamera(int cameraIdentifier, int& outCameraWidth, int& outCameraHeight) {

    // open camera stream
    cameraStreamCapture.open(cameraIdentifier);
    if (!cameraStreamCapture.isOpened())
        return -1;

    outCameraWidth = cameraStreamCapture.get(CAP_PROP_FRAME_WIDTH);
    outCameraHeight = cameraStreamCapture.get(CAP_PROP_FRAME_HEIGHT);

    return 0;
}*/


void processImage(Color32** raw, int width, int height) {

    // convert Color32 rgba image data to opencv Mat
    Mat frame(height, width, CV_8UC4, *raw);        // CV_8UC4 type is 32-bit RGBA


    flip(frame, frame, 1);

    // get center coordinates
    Point2f center(width / 2, height / 2);

    // create rotation matrix
    Mat rot_matrix = getRotationMatrix2D(center, 20.0, 1.0);

    // rotate image
    Mat frame_rot;
    warpAffine(frame, frame, rot_matrix, frame.size());

    //frame = frame_rot;
    // show result
    imshow("test image", frame);

    flip(frame, frame, 0);
}



/*** Utilities ***/

void rotateScaleImg(Mat& img, float angle = 0.0, float scale = 1.0) {

    // get center coordinates
    image_center_point = Point2f(img.size().width / 2, img.size().height / 2);

    if (angle == 0.0 && scale == 1.0) {
        return;
    }

    // create rotation matrix
    rotation_matrix = getRotationMatrix2D(image_center_point, angle, scale);

    // rotate image
    output_img.create(img.size(), img.type());
    warpAffine(img, output_img, rotation_matrix, img.size());
    img = output_img;
}



void flipImg(Mat& img, bool x_axis, bool y_axis) {
    if (x_axis && !y_axis) {
        flip(img, img, 0);
    }
    else if (!x_axis && y_axis) {
        flip(img, img, 1);
    }
    else if (x_axis && y_axis) {
        flip(img, img, -1);
    }
}



void perspectiveWarpImg(Mat& img, const vector<Point2f> screenPoints, const vector<Point2f> worldPoints) {

    Mat transformation_matrix = getPerspectiveTransform(screenPoints, worldPoints);

    warpPerspective(img, img, transformation_matrix, img.size());
}


void undistortImg(Mat& img, const Mat& cameraMatrix, const Mat& distanceCoefficients, const Mat& undistortCameraMatrix, const Rect& newImgArea) {

    Mat undistortedImg;
  
    undistort(img, undistortedImg, cameraMatrix, distanceCoefficients, undistortCameraMatrix);

    // crop according to new area
    img = undistortedImg(newImgArea);
}


void calcUndistortionMatrix(Size imageSize, Mat& undistortCameraMatrix, Rect& newImgArea, const Mat& cameraMatrix, const Mat& distanceCoefficients) {

    undistortCameraMatrix = getOptimalNewCameraMatrix(cameraMatrix, distanceCoefficients, 
        imageSize, 0, imageSize, &newImgArea);
}



/*** Camera Calibration ***/

bool saveCameraCalibration(string name, const Mat& cameraMatrix, const Mat& distanceCoefficients, const Mat& undistortMatrix, const Rect& undistortArea) {
    ofstream outStream(name);

    if (outStream) {
        /* cameraMatrix */
        uint16_t rows = cameraMatrix.rows;
        uint16_t cols = cameraMatrix.cols;

        outStream << rows << endl;
        outStream << cols << endl;

        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                double value = cameraMatrix.at<double>(r, c);
                outStream << value << endl;
            }
        }

        /* distanceCoefficients */
        rows = distanceCoefficients.rows;
        cols = distanceCoefficients.cols;

        outStream << rows << endl;
        outStream << cols << endl;

        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                double value = distanceCoefficients.at<double>(r, c);
                outStream << value << endl;
            }
        }

        /* undistortMatrix */
        rows = undistortMatrix.rows;
        cols = undistortMatrix.cols;

        outStream << rows << endl;
        outStream << cols << endl;

        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                double value = undistortMatrix.at<double>(r, c);
                outStream << value << endl;
            }
        }

        /* undistortArea */
        outStream << undistortArea.x << endl;
        outStream << undistortArea.y << endl;
        outStream << undistortArea.width << endl;
        outStream << undistortArea.height << endl;


        outStream.close();
        return true;
    }
    return false;
}


bool loadCameraCalibration(string name, Mat& cameraMatrix, Mat& distanceCoefficients, Mat& undistortMatrix, Rect& undistortArea) {
    ifstream inStream(name);

    if (inStream) {
        uint16_t rows;
        uint16_t cols;

        /* cameraMatrix */
        inStream >> rows;
        inStream >> cols;

        cameraMatrix = Mat(Size(cols, rows), CV_64F);

        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                double tmp = 0.0f;
                inStream >> tmp;
                cameraMatrix.at<double>(r, c) = tmp;
                cout << cameraMatrix.at<double>(r, c) << endl;
            }
        }

        /* distanceCoefficients */
        inStream >> rows;
        inStream >> cols;

        distanceCoefficients = Mat::zeros(rows, cols, CV_64F);

        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                double tmp = 0.0f;
                inStream >> tmp;
                distanceCoefficients.at<double>(r, c) = tmp;
                cout << distanceCoefficients.at<double>(r, c) << endl;
            }
        }

        /* undistortMatrix */
        inStream >> rows;
        inStream >> cols;

        undistortMatrix = Mat(Size(cols, rows), CV_64F);

        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                double tmp = 0.0f;
                inStream >> tmp;
                undistortMatrix.at<double>(r, c) = tmp;
                cout << undistortMatrix.at<double>(r, c) << endl;
            }
        }

        /* undistortArea */
        inStream >> undistortArea.x;
        inStream >> undistortArea.y;
        inStream >> undistortArea.width;
        inStream >> undistortArea.height;


        inStream.close();
        return true;
    }
    return false;
}

void createKnownBoardPosition(Size boardSize, float squareEdgeLength, vector<Point3f>& corners) {
    for (int i = 0; i < boardSize.height; i++) {
        for (int j = 0; j < boardSize.width; j++) {
            corners.push_back(Point3f(j * squareEdgeLength, i * squareEdgeLength, 0.0f));
        }
    }
}

void getChessboardCorners(vector<Mat> images, const Size chessboardDimensions, vector<vector<Point2f>>& allFoundCorners, bool showResults = false) {
    for (vector<Mat>::iterator iter = images.begin(); iter != images.end(); iter++) {
        vector<Point2f> pointBuf;

        bool found = findChessboardCorners (*iter, chessboardDimensions, pointBuf, CALIB_CB_ADAPTIVE_THRESH | CALIB_CB_NORMALIZE_IMAGE);

        if (found) {
            allFoundCorners.push_back(pointBuf);
        }

        if (showResults) {
            drawChessboardCorners(*iter, chessboardDimensions, pointBuf, found);
            imshow("Camera Calibration", *iter);
            waitKey(0);
        }
    }
}


void cameraCalibration(vector<Mat> calibrationImages, Size boardSize, float squareEdgeLength, Mat& cameraMatrix, Mat& distanceCoefficients, const Size chessboardDimensions = Size(6, 9)) {

    vector<vector<Point2f>> checkerboardImageSpacePoints;
    getChessboardCorners(calibrationImages, chessboardDimensions, checkerboardImageSpacePoints, false);

    vector<vector<Point3f>> worldSpaceCornerPoints;

    createKnownBoardPosition(boardSize, squareEdgeLength, worldSpaceCornerPoints[0]);
    worldSpaceCornerPoints.resize(checkerboardImageSpacePoints.size(), worldSpaceCornerPoints[0]);

    vector<Mat> rVectors, tVectors;
    distanceCoefficients = Mat::zeros(8, 1, CV_64F);

    calibrateCamera(worldSpaceCornerPoints, checkerboardImageSpacePoints, boardSize, cameraMatrix, distanceCoefficients, rVectors, tVectors);
}


void cameraCalibrationProcess(String imageDir, String cameraCalibrationFilePath, Mat& cameraMatrix, Mat& distanceCoefficients, Mat& undistortMatrix, Rect& undistortArea) {

    Mat frame;
    Mat drawToFrame;

    vector<Mat> savedImages;
    vector<vector<Point2f>> markerCorners, rejectedCandidates;

    VideoCapture video(0);

    if (!video.isOpened())
        return;

    int framesPerSecond = 20;

    String windowName = "Camera Calibration Process";
    namedWindow(windowName, WINDOW_AUTOSIZE);

    while (true) {
        if (!video.read(frame))
            break;

        vector<Vec2f> foundPoints;
        bool found = false;

        // chessboard parameters
        Size chessboardDimensions = Size(6, 9);
        const float calibrationSquareDimension = 0.006f;    // meters


        found = findChessboardCorners(frame, chessboardDimensions, foundPoints, 
            CALIB_CB_ADAPTIVE_THRESH | CALIB_CB_NORMALIZE_IMAGE | CALIB_CB_FAST_CHECK); // TODO: Fast Check macht den Prozess deutlich schneller, kann aber Schwierigkeiten mit komischen Winkeln haben
    
        frame.copyTo(drawToFrame);
        drawChessboardCorners(drawToFrame, chessboardDimensions, foundPoints, found);

        if (found)
            imshow(windowName, drawToFrame);
        else
            imshow(windowName, frame);

        char character = waitKey(1000 / framesPerSecond);

        switch (character) {
        case ' ':
            // save image on space
            if (found) {
                Mat tmp;
                frame.copyTo(tmp);
                savedImages.push_back(tmp);
            }
            break;

        case 13:
            // start calibration on enter
            //TODO: best case mind. 50 Bilder (verschiedenste Positionen & Winkel) f�r gute Kalibrierung
            if (savedImages.size() > 10) {
                cameraCalibration(savedImages, chessboardDimensions, calibrationSquareDimension, cameraMatrix, distanceCoefficients);
                calcUndistortionMatrix(savedImages[0].size(), undistortMatrix, undistortArea, cameraMatrix, distanceCoefficients);
                saveCameraCalibration(cameraCalibrationFilePath, cameraMatrix, distanceCoefficients, undistortMatrix, undistortArea);
            }
            break;

        case 27:
            // exit on escape
            loadCameraCalibration(cameraCalibrationFilePath, cameraMatrix, distanceCoefficients, undistortMatrix, undistortArea);
            break;

        }
    }
}


void findChessboardCorners(Mat& img) {

    vector<Vec2f> foundPoints;
    bool found = false;

    // chessboard parameters
    Size chessboardDimensions = Size(6, 9);
    const float calibrationSquareDimension = 0.006f;    // meters

    found = findChessboardCorners(img, chessboardDimensions, foundPoints,
        CALIB_CB_ADAPTIVE_THRESH | CALIB_CB_NORMALIZE_IMAGE | CALIB_CB_FAST_CHECK); // TODO: Fast Check macht den Prozess deutlich schneller, kann aber Schwierigkeiten mit komischen Winkeln haben

    drawChessboardCorners(img, chessboardDimensions, foundPoints, found);
}





/*** Wrapper for Unity - Calibration ***/



bool startCameraCalibration(const char* imageDir, const char* cameraCalibrationFilePath, const int imageWidth, const int imageHeight) {

    //TODO: make global (or load from file)
    Size chessboardDimensions = Size(6, 9);
    const float calibrationSquareDimension = 0.006f;    // meters


    Mat cameraMatrix;
    Mat distanceCoefficients;
    Mat undistortMatrix;
    Rect undistortArea;


    // load images
    vector<Mat> savedImages;

    vector<String> imageNames;
    String path;
    path.assign(imageDir);
    glob(path + "/*.png", imageNames);

    for (const String& imageName : imageNames) {
        Mat image = imread(imageName);
        if (!image.empty()) {
            savedImages.push_back(image);
        }
    }


    cameraCalibration(savedImages, chessboardDimensions, calibrationSquareDimension, cameraMatrix, distanceCoefficients);
    calcUndistortionMatrix(savedImages[0].size(), undistortMatrix, undistortArea, cameraMatrix, distanceCoefficients);
    saveCameraCalibration(cameraCalibrationFilePath, cameraMatrix, distanceCoefficients, undistortMatrix, undistortArea);

    return true;    //TODO: return success
}


void findChessboardCorners(Color32** rawImg, const int width, const int height) {

    // convert Color32 rgba image data to opencv Mat
    Mat img(height, width, CV_8UC4, *rawImg);

    findChessboardCorners(img);
}



void loadCalibrationParameters(const String filePath, Mat& cameraMatrix, Mat& distanceCoefficients, Mat& undistortMatrix, Rect& undistortArea) {
    loadCameraCalibration(filePath, cameraMatrix, distanceCoefficients, undistortMatrix, undistortArea);
}


void correctImg(Color32** rawImg, const int width, const int height, const string cameraCalibrationPath, const float* worldPoints, const int worldPointsLength, const float* screenPoints, const int screenPointsLength) {

    // convert Color32 rgba image data to opencv Mat
    Mat img(height, width, CV_8UC4, *rawImg);


    // convert screen & world point arrays to vectors
    vector<Point2f> screenPointVector;
    vector<Point2f> worldPointVector;

    if (worldPointsLength == 8 && screenPointsLength == 8) {

        screenPointVector.push_back(Point2f(screenPoints[0], screenPoints[1]));
        screenPointVector.push_back(Point2f(screenPoints[2], screenPoints[3]));
        screenPointVector.push_back(Point2f(screenPoints[4], screenPoints[5]));
        screenPointVector.push_back(Point2f(screenPoints[6], screenPoints[7]));

        worldPointVector.push_back(Point2f(worldPoints[0], worldPoints[1]));
        worldPointVector.push_back(Point2f(worldPoints[2], worldPoints[3]));
        worldPointVector.push_back(Point2f(worldPoints[4], worldPoints[5]));
        worldPointVector.push_back(Point2f(worldPoints[6], worldPoints[7]));
    }
    else
        return;



    // load camera parameters
    Mat cameraMatrix;
    Mat distanceCoefficients;
    Mat undistortMatrix;
    Rect undistortArea;

    loadCameraCalibration(cameraCalibrationPath, cameraMatrix, distanceCoefficients, undistortMatrix, undistortArea);


    // undistortion
    undistortImg(img, cameraMatrix, distanceCoefficients, undistortMatrix, undistortArea);

    // perspective correction
    perspectiveWarpImg(img, screenPointVector, worldPointVector);

    // rotation correction (TODO if necessary)
}


/*** Wrapper for Unity - Utilities ***/


void rotateScaleImg(Color32** rawImg, const int width, const int height, const float angle, const float scale) {

    // convert Color32 rgba image data to opencv Mat
    Mat img(height, width, CV_8UC4, *rawImg);

    // get center coordinates
    //Point2f center(img.size().width / 2, img.size().height / 2);

    // create rotation matrix
    //Mat rot_matrix = getRotationMatrix2D(center, angle, scale);

    // rotate image
    //warpAffine(img, img, rot_matrix, img.size());

    rotateScaleImg(img, angle, scale);
}


void flipImg(Color32** rawImg, const int width, const int height, const bool x_axis, const bool y_axis) {

    // convert Color32 rgba image data to opencv Mat
    Mat img(height, width, CV_8UC4, *rawImg);

    flipImg(img, x_axis, y_axis);
}



void perspectiveWarpImg(Color32** rawImg, const int width, const int height, const float* worldPoints, const int worldPointsLength, const float* screenPoints, const int screenPointsLength) {

    // convert Color32 rgba image data to opencv Mat
    Mat img(height, width, CV_8UC4, *rawImg);

    // convert screen & world point arrays to vectors
    vector<Point2f> screenPointVector;
    vector<Point2f> worldPointVector;

    if (worldPointsLength == 8 && screenPointsLength == 8) {

        screenPointVector.push_back(Point2f(screenPoints[0], screenPoints[1]));
        screenPointVector.push_back(Point2f(screenPoints[2], screenPoints[3]));
        screenPointVector.push_back(Point2f(screenPoints[4], screenPoints[5]));
        screenPointVector.push_back(Point2f(screenPoints[6], screenPoints[7]));

        worldPointVector.push_back(Point2f(worldPoints[0], worldPoints[1]));
        worldPointVector.push_back(Point2f(worldPoints[2], worldPoints[3]));
        worldPointVector.push_back(Point2f(worldPoints[4], worldPoints[5]));
        worldPointVector.push_back(Point2f(worldPoints[6], worldPoints[7]));
    }
    else
        return;

    ostringstream string;

    for (const auto& point : screenPointVector) {
        string << "(" << round(point.x*100)/100 << "," << round(point.y*100)/100 << ")";
    }
    for (const auto& point : worldPointVector) {
        string << "(" << point.x << "," << point.y << ")";
    }

    perspectiveWarpImg(img, screenPointVector, worldPointVector);
}




/*


Mat load_image(String file_path, ImreadModes mode) {
    Mat image = imread(file_path, mode);
    if (image.empty()) {
        throw "Image not found";
    }

    // resize image
    double scale_factor = 800.0 / image.rows;
    Mat image_scaled;
    resize(image, image_scaled, Size(), scale_factor, scale_factor, INTER_LINEAR);
    image = image_scaled;


    return image;
}

Mat load_image(String file_path) {
    Mat image = load_image(file_path, cv::IMREAD_UNCHANGED);
    return image;
}


void show_image(Mat image, String windowName = "") {
    // create window
    namedWindow(windowName);

    // show image
    imshow(windowName, image);

    waitKey(0);
    destroyWindow(windowName);
}

Mat change_rotation(Mat image, double angle) {
    //TODO: work on pointer to image MAT

    // get center coordinates
    Point2f center((image.cols - 1) / 2.0, (image.rows - 1) / 2.0);

    // create rotation matrix
    Mat rotation_matrix;

    rotation_matrix = getRotationMatrix2D(center, angle, 1.0);

    // rotate image
    Mat image_rot;
    warpAffine(image, image_rot, rotation_matrix, image.size());

    return image_rot;
}


int pluginTest(string windowName) {

    try
    {
        Mat image = load_image("test_image.png", IMREAD_COLOR);

        image = change_rotation(image, 20.0);
        // show image
        show_image(image, windowName);
    }
    catch (const char* msg)
    {
        printf(msg);
        return 1;
    }

    return 0;
}

*/




/*** Marker Detection ***/



/*** assuming that there is only one marker / the first marker found is used
 *** cornerPoints represents the corners of the marker: top left, top right, bottom right, bottom left.
 *** markerId returns the detected aruco ID (aruco original dictionary) ***/
void getMarkerCoordinates(Mat& baseImage, int& markerId, vector<Point2f>& cornerPoints) {

    // Define ArUco dictionary and parameters
    // 
    aruco::Dictionary arucoDictionary = aruco::getPredefinedDictionary(aruco::DICT_ARUCO_ORIGINAL);
    aruco::DetectorParameters arucoParameters = aruco::DetectorParameters();
    aruco::ArucoDetector arucoDetector(arucoDictionary, arucoParameters);

    // Detect markers
    vector<int> markerIds(10);
    vector<vector<Point2f>> markerCorners(10, std::vector<Point2f>(4)), rejected(10, std::vector<Point2f>(4));

    //aruco::detectMarkers(baseImage, arucoDictionary, cornerPoints, markerIds, arucoParameters, rejected);
    arucoDetector.detectMarkers(baseImage, markerCorners, markerIds, rejected);



    // Get marker corner coordinates
    if (markerIds.size() == 0)
    {
        //cerr << "No markers found" << endl;
        return;
    }

    // return results
    //markerId = markerIds[0];
    markerId = 42;
    //cornerPoints = markerCorners[0];

    //cout << "Marker ID: " << markerId << endl;
    //cout << "Corner Coordinates (TL - TR - BR - BL): ";
    //cout << "(" << cornerPoints[0] << ") - (" << cornerPoints[1] << ") - (" << cornerPoints[2] << ") - (" << cornerPoints[3] << ")" << endl << endl;

    return;
}



//void getMarkerCoordinates(Color32** rawImg, const int width, const int height, int* markerId, Coordinate cornerTL, Coordinate cornerTR, Coordinate cornerBR, Coordinate cornerBL) {
void getMarkerCoordinates(Color32 * *rawImg, const int width, const int height, int* markerId, float* TR_x, float* TR_y) {

    // convert Color32 rgba image data to opencv Mat
    Mat img(height, width, CV_8UC4, *rawImg);

    vector<Point2f> cornerPointsVector(4);



    getMarkerCoordinates(img, *markerId, cornerPointsVector);

    
    *TR_x = cornerPointsVector.at(1).x;
    *TR_y = cornerPointsVector.at(1).y;
    return;
}