using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class VideoController : MonoBehaviour
{
    private WebCamTexture webcam;
    
    private Renderer renderer;
    private Texture2D videoStreamTexture;

    public GameObject Calibration;
    private float[] screenPoints;
    private float[] worldPoints;
    private bool perspectiveReady = false;
    private float rotationAngle = 0.0f;
    private float scaleFactor = 1.0f;

    private string cameraParametersFilePath;
    private string calibrationImageDir;
    private int calibrationImageCounter = 0;
    private bool calibrationOngoing = false;


    public GameObject CalibrationGridPrefab;
    public GameObject CalibrationGridHolder;
    private bool calibrationGridVisible = false;
    private List<GameObject> calibrationGrid;




    private void Start() {

        /* prepare webcam & image rendering*/
        WebCamDevice[] devices = WebCamTexture.devices;
        for (int i = 0; i < devices.Length; i++)
            Debug.Log(devices[i].name);
        
    	webcam = new WebCamTexture ();

        renderer = GetComponent<Renderer>();
        renderer.material.mainTexture = webcam;

        webcam.Play();

        videoStreamTexture = new Texture2D(webcam.width, webcam.height);


        /* prepare image manipulation */
        perspectiveReady = false;

        worldPoints = Calibration.GetComponent<CalibrationScript>().getCornerPoints();
        screenPoints = worldPoints;


        /* prepare persistent storage for the camera parameters (used by the opencv library) */
        cameraParametersFilePath = Application.persistentDataPath + "/cameraParameters.txt";
        if(!File.Exists(cameraParametersFilePath)) {
            File.Create(cameraParametersFilePath).Close();
        }


        calibrationImageDir = Application.persistentDataPath + "/calibrationImages";
        if (!Directory.Exists(calibrationImageDir)) {
            Directory.CreateDirectory(calibrationImageDir);
        }

        calibrationGrid = new List<GameObject>();
	}

    private void Update() {


        if (webcam.isPlaying) {
			Color32[] rawImg = webcam.GetPixels32();
			System.Array.Reverse(rawImg);
            OpenCVLibraryWrapper.flipImg(ref rawImg, webcam.width, webcam.height, true, true);
            OpenCVLibraryWrapper.rotateScaleImg(ref rawImg, webcam.width, webcam.height, rotationAngle, scaleFactor);
           
           if (calibrationOngoing) {
                OpenCVLibraryWrapper.findChessboardCorners(ref rawImg, webcam.width, webcam.height);
           }

           if (perspectiveReady) {
                OpenCVLibraryWrapper.perspectiveWarpImg(ref rawImg, webcam.width, webcam.height, worldPoints, worldPoints.Length, screenPoints, screenPoints.Length);
            }
            
            videoStreamTexture.SetPixels32(rawImg, 0);
            videoStreamTexture.Apply();

            renderer.material.mainTexture = videoStreamTexture;
		}	
        
    }

    public void OnShowCalibrationGrid() {

        if (!calibrationGridVisible) {
        
            Vector3 areaSize = transform.parent.localScale;
            int length = 50;
            //areaSize = new Vector3(180, 180, 0);
            Debug.Log("Parent Transform: " + this.transform.parent.name + " - " + areaSize);

            for (int i = length/2; i < areaSize.x/2; i+=length)
            {
                for (int j = length/2; j < areaSize.y/2; j+=length)
                {
                    calibrationGrid.Add(Instantiate(CalibrationGridPrefab, new Vector3(i, j, -1), Quaternion.identity, CalibrationGridHolder.transform));
                    calibrationGrid.Add(Instantiate(CalibrationGridPrefab, new Vector3(-i, j, -1), Quaternion.identity, CalibrationGridHolder.transform));
                    calibrationGrid.Add(Instantiate(CalibrationGridPrefab, new Vector3(-i, -j, -1), Quaternion.identity, CalibrationGridHolder.transform));
                    calibrationGrid.Add(Instantiate(CalibrationGridPrefab, new Vector3(i, -j, -1), Quaternion.identity, CalibrationGridHolder.transform));
                }
            }

            calibrationGridVisible = true;
        }
        else {
            // make the grid invisible
            calibrationGridVisible = false;

            foreach (GameObject item in calibrationGrid)
            {
                Destroy(item);
            }

            calibrationGridVisible = false;
        }

    }


    public void OnPerspectiveButtonClick() {
        float[][] points = Calibration.GetComponent<CalibrationScript>().getPerspectiveWarpPoints();
        //TODO: check & evtl. tauschen
        screenPoints = points[0];
        worldPoints = points[1];

        perspectiveReady = true;

        Debug.Log("width, height: " + webcam.width + "; " + webcam.height);
        
        Debug.Log("worldPoints: (Length: " + worldPoints.Length + ")");
        for (int i = 0; i < worldPoints.Length; i+=2)
        {
            Debug.Log(worldPoints[i] + ", " + worldPoints[i+1]);
        }
        
        Debug.Log("screenPoints: (Length: " + screenPoints.Length + ")");
        for (int i = 0; i < screenPoints.Length; i+=2)
        {
            Debug.Log(screenPoints[i] + ", " + screenPoints[i+1]);
        }
    }

    public void OnCalibrateButtonClick() {
        calibrationOngoing = false;
        OpenCVLibraryWrapper.startCameraCalibration(calibrationImageDir, cameraParametersFilePath, webcam.width, webcam.height);
    }

    public void OnSaveForCalibrationButtonClick() {

        Color[] pixels = webcam.GetPixels();

        Texture2D texture = new Texture2D(webcam.width, webcam.height);

        texture.SetPixels(pixels);
        texture.Apply();

        byte[] bytes = texture.EncodeToPNG();
        File.WriteAllBytes($"{calibrationImageDir}/calibration_frame_{calibrationImageCounter}.png", bytes);
        Debug.Log($"{calibrationImageDir}/calibration_frame_{calibrationImageCounter}.png");

        calibrationImageCounter++;
    }

    public void OnStartCalibrationButtonClick() {
        calibrationOngoing = true;
    }

    public void OnAngleSliderChanged(float Value) {
        rotationAngle = Value;
    }


    public void OnScaleSliderChanged(float Value) {
        scaleFactor = Value;
        Debug.Log("Angle, Scale: " + rotationAngle + ", " + scaleFactor);
    }

}



