using UnityEngine;
using System.Runtime.InteropServices;

internal static class OpenCVLibraryWrapper
{
    [DllImport("OpenCVDll")]
    internal static extern void processImage(ref Color32[] raw, int width, int height);

/*** Camera Calibration ***/


/*** Utilities ***/

    [DllImport("OpenCVDll")]
    internal static extern void flipImg(ref Color32[] rawImg, int width, int height, bool x_axis, bool y_axis); 


    [DllImport("OpenCVDll")]
    internal static extern void rotateScaleImg(ref Color32[] rawImg, int width, int height, float angle = 0.0f, float scale = 1.0f);


    [DllImport("OpenCVDll")]
    internal static extern void perspectiveWarpImg(ref Color32[] rawImg, int width, int height, float[] worldPoints, int worldPointsLength, float[] screenPoints, int screenPointsLength);

    [DllImport("OpenCVDll")]
    internal static extern bool startCameraCalibration(string imageDir, string cameraCalibrationPath, int imageWidth, int imageHeight);
    
    [DllImport("OpenCVDll")]
    internal static extern bool findChessboardCorners(ref Color32[] rawImg, int width, int height);
}

