using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DrawingConfiguratorOffline : MonoBehaviour
{

    [SerializeField] private Slider mainSlider;
    [SerializeField] private float mPenThickness;
    [SerializeField] private Color mPenColor;
    public List<Toggle> mToggles;
    public ColorToggleConfigurator mColorToggleConfigurator;
    private void Awake()
    {
        mPenThickness = .5f;
        mPenThickness = 0.08f;
        mPenColor = Color.white;
    }

    public Color GetPenColor()
    {
        return mPenColor;
    }

    public float GetPenThickness()
    {
        return mPenThickness;
    }
    private void Start()
    {
        mainSlider.onValueChanged.AddListener(delegate { OnSliderValueChanged(); });
        foreach (var toggle in mToggles)
        {
            toggle.onValueChanged.AddListener(delegate {
                ToggleValueChanged(toggle);
            });
        }
    }
    public void OnSliderValueChanged()
    {
        mPenThickness = mainSlider.value * 0.002f;
    }

    public void ToggleValueChanged(Toggle toggle)
    {
        mPenColor = toggle.colors.normalColor;
        mColorToggleConfigurator.ApplyColorChanges(mPenColor);
    }
}
