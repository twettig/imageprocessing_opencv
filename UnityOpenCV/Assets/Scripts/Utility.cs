﻿//using Unity.Netcode;
using UnityEditor;
using UnityEngine;

namespace Assets
{
    public static class Utility
    {
/*         public static Vector3[] NetworkListToArray(NetworkList<Vector3> networkList)
        {
            Vector3[] tmpPositions = new Vector3[networkList.Count];
            for (int i = 0; i < networkList.Count; i++)
            {
                tmpPositions[i] = networkList[i];
            }
            return tmpPositions;
        } */


        public static bool IsInside(Collider c, Vector3 point)
        {
            return c.ClosestPoint(point) == point;
        }
    }
}