using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WritableAreaController : MonoBehaviour
{
    private Bounds bounds;

    private void Start() {
        Renderer renderer = GetComponent<Renderer>();

        bounds = renderer.bounds;

        // Print the corner coordinates
        /* Debug.Log("Top Left Corner: " + bounds.min);
        Debug.Log("Top Right Corner: (" + bounds.max.x + ", " + bounds.min.y + ", 0.00)");
        Debug.Log("Bottom Left Corner: (" + bounds.min.x + ", " + bounds.max.y + ", 0.00)");
        Debug.Log("Bottom Right Corner: " + bounds.min); */
    }

/*     public Vector2[] getCorners2D() {

        Vector2[] cornerPositions = new Vector2[] {
            new Vector2(bounds.min.x, bounds.max.y),
            new Vector2(bounds.max.x, bounds.max.y),
            new Vector2(bounds.min.x, bounds.min.y),
            new Vector2(bounds.max.x, bounds.min.y),
        };
        
        // Print the corner coordinates
        Debug.Log("Top Left Corner: " + bounds.min);
        Debug.Log("Top Right Corner: (" + bounds.max.x + ", " + bounds.min.y + ", 0.00)");
        Debug.Log("Bottom Left Corner: (" + bounds.min.x + ", " + bounds.max.y + ", 0.00)");
        Debug.Log("Bottom Right Corner: " + bounds.min);

        return cornerPositions;
    } */
}
