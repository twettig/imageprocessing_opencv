using UnityEngine;
using System.Runtime.InteropServices;

internal static class ImageProcessing_OpenCV_Wrapper
{
    [DllImport("ImageProcessing_OpenCV_API")]
    internal static extern void processImage(ref Color32[] raw, int width, int height);
}