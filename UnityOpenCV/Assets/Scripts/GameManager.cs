using System.Collections;
using System.Collections.Generic;
//using System.Threading.Tasks;
//using Unity.Netcode.Transports.UTP;
//using Unity.Services.Authentication;
//using Unity.Services.Core;
//using Unity.Services.Relay;
//using Unity.Services.Relay.Models;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    
    [SerializeField] private int SCREEN_WIDTH = 1280;
    [SerializeField] private int SCREEN_HEIGHT = 800;
    [SerializeField] private GameObject uiObject;

    [HideInInspector] public bool mGameMode;
    private bool mIsFullscreen = false;
    // Start is called before the first frame update


    //private UnityTransport mTransport;

    ////private async void Awake()
    ////{
    ////    mTransport = FindObjectOfType<UnityTransport>();
    ////    await Authenticate();
    ////}

    //private async Task Authenticate()
    //{
    //    await UnityServices.InitializeAsync();
    //    await AuthenticationService.Instance.SignInAnonymouslyAsync();
    //}
    void Start()
    {

        Screen.SetResolution(SCREEN_WIDTH, SCREEN_HEIGHT, mIsFullscreen);

    }

    //public async void CreateGame()
    //{
    //    Allocation a = await RelayService.Instance.CreateAllocationAsync(2);
    //    joinCode.text = await RelayService.Instance.GetJoinCodeAsync(a.AllocationId);
    //    mTransport.SetHostRelayData(a.RelayServer.IpV4, (ushort)a.RelayServer.Port, a.AllocationIdBytes, a.Key, a.ConnectionData);
           
    //}

    //public async void JoinGame()
    //{
    //    var a = await RelayService.Instance.JoinAllocationAsync(joinCode.text);
    //    mTransport.SetClientRelayData(a.RelayServer.IpV4, (ushort)a.RelayServer.Port, a.AllocationIdBytes, a.Key, a.ConnectionData, a.HostConnectionData);
    //}


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            mIsFullscreen = !mIsFullscreen;
            Screen.SetResolution(SCREEN_WIDTH, SCREEN_HEIGHT, mIsFullscreen);
        }


        if (Input.GetKeyDown(KeyCode.T))
        {
            mGameMode = !mGameMode;
        }

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            uiObject.SetActive(!uiObject.activeSelf);
        }
    }
}
