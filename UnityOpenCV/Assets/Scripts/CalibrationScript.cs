using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalibrationScript : MonoBehaviour
{

    public GameObject writableArea;
    //TODO:
    //camera matrix
    //distance coefficients
    //undistortion matrix
    

    public float[][] getPerspectiveWarpPoints() {

        // get marker positions / screen points TODO: check
        GameObject markerTopLeft = transform.Find("Marker_TopLeft").gameObject;
        GameObject markerTopRight = transform.Find("Marker_TopRight").gameObject;
        GameObject markerBottomLeft = transform.Find("Marker_BottomLeft").gameObject;
        GameObject markerBottomRight = transform.Find("Marker_BottomRight").gameObject;

        Vector2[] markerPositions = new Vector2[] {
            markerTopLeft.GetComponent<DragAndDrop>().getMarkerPosition2D(),
            markerTopRight.GetComponent<DragAndDrop>().getMarkerPosition2D(),
            markerBottomLeft.GetComponent<DragAndDrop>().getMarkerPosition2D(),
            markerBottomRight.GetComponent<DragAndDrop>().getMarkerPosition2D()
        };

        // get corner positions / world points TODO: check
        //Vector2[] cornerPositions = writableArea.GetComponent<WritableAreaController>().getCorners2D();
        // get marker positions / screen points TODO: check
        GameObject cornerTopLeft = transform.Find("Corner_TopLeft").gameObject;
        GameObject cornerTopRight = transform.Find("Corner_TopRight").gameObject;
        GameObject cornerBottomLeft = transform.Find("Corner_BottomLeft").gameObject;
        GameObject cornerBottomRight = transform.Find("Corner_BottomRight").gameObject;

        Vector2[] cornerPositions = new Vector2[] {
            new Vector2(cornerTopLeft.transform.position.x, cornerTopLeft.transform.position.y),
            new Vector2(cornerTopRight.transform.position.x, cornerTopRight.transform.position.y),
            new Vector2(cornerBottomLeft.transform.position.x, cornerBottomLeft.transform.position.y),
            new Vector2(cornerBottomRight.transform.position.x, cornerBottomRight.transform.position.y),
        };


        // call calibration in opencv
        float[] markerPoints = vector2ToFloatArray(markerPositions);
        float[] cornerPoints = vector2ToFloatArray(cornerPositions);

        return new float[][] {markerPoints, cornerPoints};
    }


    public float[] getCornerPoints() {
        GameObject cornerTopLeft = transform.Find("Corner_TopLeft").gameObject;
        GameObject cornerTopRight = transform.Find("Corner_TopRight").gameObject;
        GameObject cornerBottomLeft = transform.Find("Corner_BottomLeft").gameObject;
        GameObject cornerBottomRight = transform.Find("Corner_BottomRight").gameObject;

        Vector2[] cornerPositions = new Vector2[] {
            new Vector2(cornerTopLeft.transform.position.x, cornerTopLeft.transform.position.y),
            new Vector2(cornerTopRight.transform.position.x, cornerTopRight.transform.position.y),
            new Vector2(cornerBottomLeft.transform.position.x, cornerBottomLeft.transform.position.y),
            new Vector2(cornerBottomRight.transform.position.x, cornerBottomRight.transform.position.y),
        };

        return vector2ToFloatArray(cornerPositions);
    }


    private float[] vector2ToFloatArray(Vector2[] src) {
        
        float[] dest = new float[src.Length * 2];
        int i = 0;

        foreach (Vector2 entry in src)
        {
            dest[i] = entry.x;
            dest[i+1] = entry.y;

            i += 2;
        }

        return dest;
    }
}
