using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorToggleConfigurator : MonoBehaviour
{
    public Color32 mToggleColor;

    private void Start()
    {
        ApplyColorChanges(mToggleColor);
    }

    public void ApplyColorChanges(Color32 color)
    {
        ColorBlock colorBlock = this.GetComponent<Toggle>().colors;
        colorBlock.highlightedColor = color;
        colorBlock.normalColor = color;
        colorBlock.disabledColor = color;
        colorBlock.pressedColor = color;
        colorBlock.selectedColor = color;
        this.GetComponent<Toggle>().colors = colorBlock;

        Image[] images = GetComponentsInChildren<Image>();
        foreach (var img in images)
        {
            img.color = color;
        }

    }
}
