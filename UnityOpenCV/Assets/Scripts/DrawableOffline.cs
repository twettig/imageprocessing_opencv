using Assets;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class DrawableOffline : MonoBehaviour
{

    [SerializeField] private GameObject mLinePrefab;
    [SerializeField] private Color mBackgroundColor = Color.black;

    private Camera mCamera;
    private bool mIsInteractable = true;
    private int mCurrentIndex = 0;
    private GameObject mCurrentGo;
    private LineRenderer mCurrentLineRenderer;
    private List<GameObject> mGameObjectLines;
    private List<Vector3> mCurrentLRPositions;


    private void Awake()
    {
        var mesh = GetComponent<MeshRenderer>();
        mesh.material.color = mBackgroundColor;
    }
    void Start()
    {
        mCurrentLRPositions = new List<Vector3>();
        mGameObjectLines = new List<GameObject>();
        mCamera = Camera.main;
        mCurrentLineRenderer = null;
    }

    internal void SetLineObject(GameObject gameObject)
    {
        mCurrentGo = gameObject;
        SetupGameObject(mCurrentGo);
    }

    private void Update()
    {
        if (Keyboard.current.deleteKey.isPressed)
        {
            DeleteAllLines();
        }

        if (Keyboard.current.backspaceKey.isPressed)
        {
            DeleteLastLine();
        }

        if (Keyboard.current.spaceKey.isPressed)
        {
            RedoLastLine();
        }
    }




    private void DeleteAllLines()
    {
        foreach (var go in mGameObjectLines)
        {
            go.SetActive(false);
        }
    }

    private void RedoLastLine()
    {
        for (int i = mGameObjectLines.Count - 1; i >= 0; i--)
        {
            GameObject lastLine = mGameObjectLines[i];
            if (lastLine.activeSelf)
            {
                if (mGameObjectLines.Count - 1 >= i + 1)
                {
                    mGameObjectLines[i + 1].SetActive(true);
                }
                break;
            }
        }
    }

    private void DeleteLastLine()
    {
        for (int i = mGameObjectLines.Count - 1; i >= 0; i--)
        {
            GameObject lastLine = mGameObjectLines[i];
            if (lastLine.activeSelf)
            {
                lastLine.SetActive(false);
                break;
            }
        }
    }

    private void OnMouseDown()
    {

        if (!mIsInteractable)
        {
            return;
        }
        var go = Instantiate(mLinePrefab);
        SetupGameObject(go);
    }

    public void SetupGameObject(GameObject go)
    {
        mCurrentIndex = 0;
        mCurrentLineRenderer = null;
        mCurrentLRPositions.Clear();
        mCurrentLineRenderer = go.GetComponent<LineRenderer>();
        SetIntialPosition();
        mGameObjectLines.Add(go);
    }


    private void OnMouseDrag()
    {
        if (!mIsInteractable)
        {
            return;
        }
        Vector3 mousePos = GetNormalizedMousePosition();
        if (mCurrentLineRenderer == null) return;
        if (mousePos != mCurrentLineRenderer.GetPosition(mCurrentIndex - 1))
        {
            Vector3 comparingMousePos = mousePos;
            comparingMousePos.z = gameObject.transform.position.z;
            if (Utility.IsInside(gameObject.GetComponent<BoxCollider>(), comparingMousePos)) AddPointToLR(mousePos);
        }
    }
    private void OnMouseUp()
    {
        if (!mIsInteractable) return;
        mCurrentLineRenderer.positionCount--;
        mCurrentIndex = 0;
        mCurrentGo.GetComponent<LineRendererPlayerOffline>().SetLocalLRPositions(mCurrentLRPositions);
        mCurrentLRPositions.Clear();
        mCurrentLineRenderer = null;
    }

    private void SetIntialPosition()
    {
        Vector3 mousePos = GetNormalizedMousePosition();
        mCurrentLineRenderer.SetPosition(mCurrentIndex, mousePos);
        mCurrentLRPositions.Add(mousePos);
        mCurrentIndex++;
        mCurrentLineRenderer.positionCount--;
    }

    private void AddPointToLR(Vector3 mousePos)
    {
        mCurrentLineRenderer.positionCount++;
        mCurrentLineRenderer.SetPosition(mCurrentIndex, mousePos);
        mCurrentLRPositions.Add(mousePos);
        mCurrentIndex++;
    }

    private Vector3 GetNormalizedMousePosition()
    {
        Vector3 mousePos = mCamera.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = -1;
        return mousePos;
    }



    
}
