using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragAndDrop : MonoBehaviour
{
    private bool isDragging = false;
    private Collider2D collider;

    private void Start() {
        collider = GetComponent<Collider2D>();
    }


    private void Update() {

        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if (Input.GetMouseButtonDown(0)) {
            if (collider == Physics2D.OverlapPoint(mousePosition)) {
                isDragging = true;
                //Debug.Log(transform.position);
            }
        }

        if (isDragging) {
            transform.position = mousePosition;

            if (Input.GetMouseButtonUp(0)) {
                Debug.Log(name + " - Position: " + transform.position);
                isDragging = false;
            }
        }        
    }

    public Vector3 getMarkerPosition2D() {
        return new Vector2(transform.position.x, transform.position.y);
    }
}
