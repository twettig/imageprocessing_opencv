
using System.Collections.Generic;
using UnityEngine;

namespace Assets
{
    public class LineRendererPlayerOffline : MonoBehaviour
    {
        private List<Vector3> mPositions;
        private Material mLineMaterial;
        private LineRenderer mLR;
        private DrawingConfiguratorOffline drawingConfigurator;

        void Awake()
        {

            var gameManagerGo = GameObject.Find("GameManager");
            drawingConfigurator = gameManagerGo.GetComponent<DrawingConfiguratorOffline>();
            mPositions = new();
            mLR = this.gameObject.AddComponent<LineRenderer>();
            mLineMaterial = Resources.Load<Material>("Materials/LineMaterial");
            ConfigureLR(ref mLR);

            DrawableOffline drawable = GameObject.Find("DrawingArea").GetComponent<DrawableOffline>();
            drawable.SetLineObject(this.gameObject);
        }

        private void ConfigureLR(ref LineRenderer line)
        {
            line.startWidth = drawingConfigurator.GetPenThickness();
            line.endWidth = drawingConfigurator.GetPenThickness();
            line.startColor = drawingConfigurator.GetPenColor();
            line.endColor = drawingConfigurator.GetPenColor();
            line.material = mLineMaterial;
        }

        public void SetLocalLRPositions(List<Vector3> positions)
        {
            foreach (var currentPosition in positions) mPositions.Add(currentPosition);
        }


        public void RenderLR()
        {
            mLR.positionCount = mPositions.Count;
            mLR.SetPositions(mPositions.ToArray());
        }

        private void Update()
        {
            if (mPositions.Count > 0)
            {
                RenderLR();
            }

        }
    }
}