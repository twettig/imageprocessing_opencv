# ImageProcessing_OpenCV


## Structure

- `OpenCVDll`: Library mit Image Processing Funktionen in C++ mit OpenCV.
- `ImageProcessing_OpenCV`: Library + API. API ist ein Wrapper der Library DLL, damit Unity nicht jedesmal neu gestartet werden muss, um eine neue Version der Library zu testen. *Funktioniert aber noch nicht*.
- `UnityOpenCV`: Unity-Projekt, in dem die OpenCV Libraries verwendet werden.




## Todo

- Kamerakalibrierung in Unity verfügbar machen
- Perspektive & Rotation automatisch korrigieren
- Aufbau mit Kamera + Beamer untersuchen
- StackOverflow in ImageProcessing_OpenCV lösen (liegt wahrscheinlich an der Übergabe des Color32**)


## Bugs

- Farbfehler bei Anzeige des Streams in OpenCV. In Unity sind die Farben korrekt



## Backlog

- Bild von OpenCV an Unity übergeben
- Bild von Unity an OpenCV übergeben
- Kamerakalibrierung
    - Distortion automatisiert herausrechnen
    - OpenCV (C++)
    - Opencv (Python)
- Bildverarbeitung
    - Perspektive
    - Rotation (Winkel)
    - Skalierung
    - Spiegelung