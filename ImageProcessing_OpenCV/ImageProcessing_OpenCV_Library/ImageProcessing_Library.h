#pragma once


namespace ImageProcessing{


	struct Color32;


	//extern "C" __declspec(dllexport) void initCamera();

	extern "C" __declspec(dllexport) void processImage(Color32 * *raw, int width, int height);

}