#include "pch.h"
#include "ImageProcessing_Library.h"


#include <cstdio>
#include <format>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

using namespace std;
using namespace cv;

namespace {


    struct Color32
    {
    uchar red;
    uchar green;
    uchar blue;
    uchar alpha;
    };

    /*
    VideoCapture cameraStreamCapture;
    String windowName = "Unity_Test";


    int initCamera(int cameraIdentifier, int& outCameraWidth, int& outCameraHeight) {

    // open camera stream
    cameraStreamCapture.open(cameraIdentifier);
    if (!cameraStreamCapture.isOpened())
        return -1;

    outCameraWidth = cameraStreamCapture.get(CAP_PROP_FRAME_WIDTH);
    outCameraHeight = cameraStreamCapture.get(CAP_PROP_FRAME_HEIGHT);

    return 0;
    }*/


    void processImage(Color32** raw, int width, int height) {

    // convert Color32 rgba image data to opencv Mat
    Mat frame(height, width, CV_8UC4, *raw);        // CV_8UC4 type is 32-bit RGBA

    // get center coordinates
    Point2f center(width / 2, height / 2);

    // create rotation matrix
    Mat rot_matrix = getRotationMatrix2D(center, 45.0, 1.0);

    // rotate image
    Mat frame_rot;
    //warpAffine(frame, frame_rot, rot_matrix, frame.size());

    //frame = frame_rot;

    // show result
    imshow("test image", frame);

    }



    /*


    Mat load_image(String file_path, ImreadModes mode) {
    Mat image = imread(file_path, mode);
    if (image.empty()) {
        throw "Image not found";
    }

    // resize image
    double scale_factor = 800.0 / image.rows;
    Mat image_scaled;
    resize(image, image_scaled, Size(), scale_factor, scale_factor, INTER_LINEAR);
    image = image_scaled;


    return image;
    }

    Mat load_image(String file_path) {
    Mat image = load_image(file_path, cv::IMREAD_UNCHANGED);
    return image;
    }


    void show_image(Mat image, String windowName = "") {
    // create window
    namedWindow(windowName);

    // show image
    imshow(windowName, image);

    waitKey(0);
    destroyWindow(windowName);
    }

    Mat change_rotation(Mat image, double angle) {
    //TODO: work on pointer to image MAT

    // get center coordinates
    Point2f center((image.cols - 1) / 2.0, (image.rows - 1) / 2.0);

    // create rotation matrix
    Mat rotation_matrix;

    rotation_matrix = getRotationMatrix2D(center, angle, 1.0);

    // rotate image
    Mat image_rot;
    warpAffine(image, image_rot, rotation_matrix, image.size());

    return image_rot;
    }


    int pluginTest(string windowName) {

    try
    {
        Mat image = load_image("test_image.png", IMREAD_COLOR);

        image = change_rotation(image, 20.0);
        // show image
        show_image(image, windowName);
    }
    catch (const char* msg)
    {
        printf(msg);
        return 1;
    }

    return 0;
    }

    */
}